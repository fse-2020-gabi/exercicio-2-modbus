#include "crc/crc16.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART

#define COD_DIS 0x01

#define COD_ESC 0x16
#define COD_LE 0x23

#define COD_ESC_INT 0xB1
#define COD_ESC_FLOAT 0xB2
#define COD_ESC_CHAR 0xB3

#define COD_LE_INT 0xA1
#define COD_LE_FLOAT 0xA2
#define COD_LE_CHAR 0xA3

#define CRC_SIZE 2
#define MENS_DEFAULT_SIZE 3

// MARK: utils

void imprimir_array_uchar(unsigned char * array, char* titulo, int size){

  printf("%s", titulo);
  for (int i = 0; i < size; i++){
    printf("%d", array[i]);
  }
  printf("\n");

}

// MARK: ações UART

int abre_uart(){
  int uart0_filestream = -1;
  uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);

  if (uart0_filestream == -1){
    printf("ERRO AO INICAR UART.\n");
  }
  else {
    printf("UART INICIALIZADA!\n");
  }
  
  struct termios options;
  tcgetattr(uart0_filestream, &options);
  options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;
  tcflush(uart0_filestream, TCIFLUSH);
  tcsetattr(uart0_filestream, TCSANOW, &options);

  return uart0_filestream;
}

void escreve_uart(int uart0_filestream, unsigned char * mensagem, int tamanho_msg, unsigned char option, unsigned char sub_option){
  
    int size_codigo = MENS_DEFAULT_SIZE + tamanho_msg;
    unsigned char codigo[size_codigo];
    unsigned char default_msg[MENS_DEFAULT_SIZE] = {COD_DIS, option, sub_option};

    memcpy(codigo, default_msg, MENS_DEFAULT_SIZE);

    if (tamanho_msg != 0){
      // possui uma mensagem
      memcpy(&codigo[MENS_DEFAULT_SIZE], mensagem, tamanho_msg);
    }

    short crc = calcula_CRC(codigo, size_codigo);
    int arg_size = size_codigo + CRC_SIZE;

    unsigned char args[arg_size];
    memcpy(args, codigo, size_codigo);
    memcpy(&args[size_codigo], &crc, CRC_SIZE);


  printf("ESCREVENDO NA UART\n");
  
  int count = write(uart0_filestream, args, arg_size);

  if (count < 0){
    printf("ERRO AO ESCREVER NA UART\n");
  } else {
    printf("TAMANHO DA MENSAGEM: %d. ", count);
    imprimir_array_uchar(args, "MENSAGEM ENVIADA: ", arg_size);
  }
}

void le_uart(int uart0_filestream){
  printf("LENDO UART\n");
  unsigned char response[256];
  int size = read(uart0_filestream, response, sizeof(response)/sizeof(unsigned char));
  if (size < 0){
    printf("ERRO AO LER DADOS.\n");
  }
  else if (size == 0){
    printf("NENHUM DADO DISPONÍVEL.\n");
  }
  else{
    printf("TAMANHO DA RESPOSTA: %d. ", size);
    imprimir_array_uchar(response, "MENSAGEM RECEBIDA: ", size);
  }

}

// MARK: menu

void solicita_int(int uart0_filestream){
  escreve_uart(uart0_filestream, " ", 0,  COD_LE, COD_LE_INT);
  sleep(2);
  le_uart(uart0_filestream);
}

void solicita_float(int uart0_filestream){
  escreve_uart(uart0_filestream, " ", 0,  COD_LE, COD_LE_FLOAT);
  sleep(2);
  le_uart(uart0_filestream);
}

void solicita_char(int uart0_filestream){
  escreve_uart(uart0_filestream, " ", 0,  COD_LE, COD_LE_CHAR);
  sleep(2);
  le_uart(uart0_filestream);
}

void escreve_int(int uart0_filestream){
  char mensagem[sizeof(int)];
  int i = 1817;
  memcpy(mensagem, &i, sizeof(i));

  escreve_uart(uart0_filestream, mensagem, sizeof(mensagem)/sizeof(unsigned char), COD_ESC, COD_ESC_INT);
  sleep(2);
  le_uart(uart0_filestream);
}

void escreve_float(int uart0_filestream){
  char mensagem[sizeof(float)];
  float f = 18.17f;
  memcpy(mensagem, &f, sizeof(f));

  escreve_uart(uart0_filestream, mensagem, sizeof(mensagem)/sizeof(unsigned char), COD_ESC, COD_ESC_FLOAT);
  sleep(2);
  le_uart(uart0_filestream);
}

void escreve_char(int uart0_filestream){
  char mensagem[5] = {0x04, '1', '8', '1', '7'};
  escreve_uart(uart0_filestream, mensagem, sizeof(mensagem)/sizeof(char), COD_ESC, COD_ESC_CHAR);
  sleep(2);
  le_uart(uart0_filestream);
}

// MARK: main

int main(int argc, const char * argv[]) {

  int uart0_filestream = abre_uart();

  if (uart0_filestream != -1){
    escreve_int(uart0_filestream);
  } else {
    printf("ERRO AO ABRIR UART");
  }
  close(uart0_filestream);
  return 0;
}

// TODO Verificar erro na leitura

// scp -P 23900 main.c gabrielasilva@3.tcp.ngrok.io:~/exercicio2
// ssh gabrielasilva@3.tcp.ngrok.io -p 23900

// 160121817